# -*- coding: utf-8 -*-
import tornado.httpserver
import tornado.ioloop
import tornado.web
import logging
import bcrypt

#MainHandler class
class MainHandler(tornado.web.RequestHandler):
	def get(self):
		self.render("index.html")

#PostHander class
class PostHandler(tornado.web.RequestHandler):
    def post(self):
        self.render("chart.html")
        name = self.get_argument('name', '')
        password = self.get_argument('password', '')
        email_address = self.get_argument('email', '')

        password = password.encode('utf-8')
        hashedPassword = bcrypt.hashpw(password, bcrypt.gensalt())

        logging.info('name: ' + name)
        logging.info('password: ' + hashedPassword.decode('utf-8'))
        logging.info('email: ' + email_address)

#tornado application settings
application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/login", PostHandler),
    (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": "web/static"}),
])

if __name__ == "__main__":
    logging.basicConfig(filename='logfile.log',level=logging.DEBUG)
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8888)
    tornado.log.enable_pretty_logging()
    tornado.ioloop.IOLoop.instance().start()