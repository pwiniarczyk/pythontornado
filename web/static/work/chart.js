﻿var events_arr = [
 {
     eventprice:550000,
     moddate:"2016-11-11T17:14:17"
},
 {
     eventprice:520000,
     moddate:"2016-11-24T15:52:34"
},
 {
     eventprice:650000,
     moddate:"2016-11-29T11:21:05.923581"
}
];
// Parse the date / time
events_arr.forEach(function (d) {
    d.moddate = d3.isoParse(d.moddate);
});

// Set the dimensions of the canvas / graph
var margin = { top: 30, right: 20, bottom: 30, left: 50 },
    width = 600 - margin.left - margin.right,
    height = 270 - margin.top - margin.bottom;

// Set the ranges
var x = d3.scaleTime().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);

// Define the axes
var xAxis = d3.axisBottom(x);

var yAxis = d3.axisLeft(y);

// Define the line
var valueline = d3.line()
    .x(function (d) { return x(d.moddate); })
    .y(function (d) { return y(d.eventprice); });

// Adds the svg canvas
var svg = d3.select("#chart1")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
    .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");

    // Scale the range of the data
x.domain(d3.extent(events_arr, function (d) { return d.moddate; }));
y.domain([0, d3.max(events_arr, function (d) { return d.eventprice; })]);

    // Add the valueline path.
    svg.append("path")
        .attr("class", "line")
        .attr("d", valueline(events_arr));

    // Add the X Axis
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    // Add the Y Axis
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);